# COVID-19 visualisation

This very simple Python script downloads and shows COVID-19 cases for a number of selected countries.
It is based on making requests to https://api.covid19api.com .

## Running the script
Run virus.py

## Requirements
population.py
(list of population by country, for normalising numbers of cases)

## Bugs
The country names in population.py may be different from those expected by the API (I haven't tried all countries). Consider modifying if necessary.
