#!/usr/bin/python3
# Read and display COVID-19 data from https://api.covid19api.com - J.Cuenca 2020
#
# This program is free software and is distributed under the terms of the GNU General Public License, version 3 or any later version. A copy of the license can be found here: https://www.gnu.org/licenses/gpl-3.0.en.html

import requests
import ast
from datetime import datetime
import numpy as np
import matplotlib.pyplot as pl
import matplotlib.dates as md
from cycler import cycler
from population import *
from time import sleep 

r={}
cases={}
active={}
deaths={}
recovered={}
dates={}
time={}
newcases={}
newcasesweekavg={}
daydeaths={}
daydeathsweekavg={}

# countries=("china", "united-states", "italy", "spain", "france", "united-kingdom", "germany", "belgium", "colombia")
# countries=("france", "belgium", "colombia")
# countries=("china", "united-states", "italy", "spain", "france", "united-kingdom", "germany", "belgium", "sweden")
# countries=("china", "united-states", "italy", "spain", "france", "united-kingdom", "belgium", "colombia", "brazil")
# countries=("australia", "russia", "canada", "norway", "finland", "denmark", "germany", "belgium", "sweden")
countries=("sweden", "belgium", "finland", "france", "colombia")

for country in countries:
    r = requests.get('https://api.covid19api.com/total/country/'+country+'/status/confirmed')
    t = ast.literal_eval(r.text)
    cases[country] = np.array([t[i]["Cases"] for i in range(len(t))])
    dates[country] = np.array([t[i]["Date"] for i in range(len(t))])
    
    r = requests.get('https://api.covid19api.com/total/country/'+country+'/status/deaths')
    t = ast.literal_eval(r.text)
    deaths[country] = np.array([t[i]["Cases"] for i in range(len(t))])
    
    r = requests.get('https://api.covid19api.com/total/country/'+country+'/status/recovered')
    t = ast.literal_eval(r.text)
    recovered[country] = np.array([t[i]["Cases"] for i in range(len(t))])
    
    time[country] = np.array([int(datetime.strptime(dates[country][i],'%Y-%m-%dT%H:%M:%SZ').timestamp()) for i in range(len(t))])
    dates[country] = np.array([datetime.fromtimestamp(time[country][i]).strftime('%Y-%m-%d') for i in range(len(t))])
    active[country] = cases[country] - recovered[country] - deaths[country]
    newcases[country] = np.append(0,np.diff(cases[country]))
    newcasesweekavg[country] = np.convolve(newcases[country],np.ones((7,))/7,mode="valid")
    daydeaths[country] = np.append(0,np.diff(deaths[country]))
    daydeathsweekavg[country] = np.convolve(daydeaths[country],np.ones((7,))/7,mode="valid")
    
    sleep(1.0) # prevent api from denying frequent requests



## PLOTS ================================================================================


xdays = 28 # 7 # number of days between xticks


pl.rcdefaults()
pl.rc('lines', linewidth=2)
pl.rc('axes', prop_cycle=(cycler('color', ['k','b','g','r','c','c','m','m']) + cycler('linestyle', ['--','-','-','-','-','-','--','-'])))



fig, ax = pl.subplots(nrows=1, ncols=len(countries), sharex=True, sharey=True, figsize=(11,11))
for n in range(len(countries)):
    country = countries[n]
    p = population[countries[n]]/1e6
    ax[n].semilogy(time[country],cases[country]/p,label="total cases")
    ax[n].plot(time[country],active[country]/p,label="active")
    ax[n].plot(time[country],recovered[country]/p,label="recovered")
    ax[n].plot(time[country],deaths[country]/p,label="deaths")
    ax[n].plot(time[country],newcases[country]/p,label="daily cases",linewidth=1/4)
    ax[n].plot(time[country][len(time[country])-len(newcasesweekavg[country]):],newcasesweekavg[country]/p,label="daily cases week avg",linewidth=2)
    ax[n].plot(time[country],daydeaths[country]/p,label="daily deaths",linewidth=1/4)
    ax[n].plot(time[country][len(time[country])-len(daydeathsweekavg[country]):],daydeathsweekavg[country]/p,label="daily deaths week avg",linewidth=2)
    ax[n].set_title(country)
    ax[n].set_xticklabels([])
    ## if n>=len(countries)-ncols*nrows:
    ax[n].set_xticks(time[countries[0]][::xdays])
    ax[n].set_xticklabels(dates[countries[0]][::xdays], rotation="vertical" )
fig.suptitle("Cases per million people",x=.2)
handles, labels = ax[n].get_legend_handles_labels()
fig.legend(handles, labels, loc='upper center',prop={'size': 8})
fig.subplots_adjust(bottom=0.15,top=.8, left=.1, right=.95)
fig.savefig("cases_per_million_people__log.pdf")




pl.rcdefaults()
pl.rc('lines', linewidth=2)

fig, ax = pl.subplots(nrows=3, ncols=1, sharex=True, figsize=(8,8))
for n in range(len(countries)):
    country = countries[n]
    ax[0].plot(time[country],active[country],label=country)
ax[0].set_ylabel('active cases')
ax[0].set_xlabel('time')
ax[0].legend()
ax[0].set_ylim(bottom=1)

for n in range(len(countries)):
    country = countries[n]
    ax[1].plot(time[country],active[country]/population[country]*1e6,label=country)
ax[1].set_ylabel('active cases per million people')
ax[1].set_xlabel('time')
ax[1].legend()

for n in range(len(countries)):
    country = countries[n]
    ax[2].plot(time[country][len(time[country])-len(newcasesweekavg[country]):],newcasesweekavg[country]/population[country]*1e6,linewidth=2,label=country)
ax[2].set_ylabel('daily cases per million people')
ax[2].set_xlabel('time')
ax[2].legend()
ax[2].set_xticks(time[countries[0]][::xdays])
ax[2].set_xticklabels(dates[countries[0]][::xdays], rotation="vertical" )
fig.suptitle("Cases")
fig.subplots_adjust(bottom=0.2)
fig.savefig("cases_per_million_people.pdf")




pl.rcdefaults()
pl.rc('lines', linewidth=2)

fig, ax = pl.subplots(nrows=3, ncols=1, sharex=True, figsize=(8,8))
for n in range(len(countries)):
    country = countries[n]
    ax[0].plot(time[country],deaths[country],label=country)
ax[0].set_ylabel('Deaths')
ax[0].set_xlabel('time')
ax[0].legend()
ax[0].set_ylim(bottom=1)

for n in range(len(countries)):
    country = countries[n]
    ax[1].plot(time[country],deaths[country]/population[country]*1e6,label=country)
ax[1].set_ylabel('Deaths per million people')
ax[1].set_xlabel('time')
ax[1].legend()

for n in range(len(countries)):
    country = countries[n]
    ax[2].plot(time[country][len(time[country])-len(daydeathsweekavg[country]):],daydeathsweekavg[country]/population[country]*1e6,linewidth=2,label=country)
ax[2].set_ylabel('daily deaths per million people')
ax[2].set_xlabel('time')
ax[2].legend()
ax[2].set_xticks(time[countries[0]][::xdays])
ax[2].set_xticklabels(dates[countries[0]][::xdays], rotation="vertical" )
fig.suptitle("Deaths")
fig.subplots_adjust(bottom=0.2)
fig.savefig("deaths_per_million_people.pdf")




fig, ax = pl.subplots(nrows=2, ncols=1, sharex=False, figsize=(8,8)) # True
for n in range(len(countries)):
    country = countries[n]
    p = population[countries[n]]/1e6
    ax[0].loglog(deaths[country][len(deaths[country])-len(daydeathsweekavg[country]):]/p,daydeathsweekavg[country]/p,label=country)
    ax[1].loglog(deaths[country]/p,active[country]/p,label=country)
ax[0].legend()
ax[0].set_xlabel('deaths per million people')
ax[0].set_ylabel('daily deaths per million people (sliding week average)')
ax[0].grid(1,which="both")
ax[1].grid(1,which="both")
ax[1].set_xlabel('deaths per million people')
ax[1].set_ylabel('active cases per million people')
ax[1].legend()
fig.suptitle("Deaths per million people")
fig.savefig("daily_deaths_and_active_cases_vs_deaths.pdf")



pl.show()
pl.close('all')
